.. _api:

API
===

.. module:: database

This part of the documentation covers all the interfaces of efitai_database
utilities.  


Database Object
------------------

.. autoclass:: ExtractData
   :members:
   :inherited-members:

.. autoclass:: ExtractEquilibria
   :members:
   :inherited-members:

