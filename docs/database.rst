.. highlight:: default

Alternative access methods
==========================

Using OMAS 
----------

The EFIT-AI data uses `OMAS <https://gafusion.github.io/omas/schema/schema_equilibrium.html>`_.
to save the data into the IMAS data format as hdf5 files. The IMAS format is a very complete 
record of discharge and EFIT runs.   

If omas and the OMFIT geqdsk libraries are installed, the data can be converted
into the standard geqdsk format using `from_omas()` (here `filename` and
`time_index` are user-defined variables).::

	from omfit_classes.omfit_eqdsk import OMFITgeqdsk, OMFITsrc
	from omas import *
	ods = load_omas_h5(filename)
	eq = OMFITgeqdsk('geqdsk').from_omas(ods,time_index=time_index)

Using h5py directly
----------------------

For quick examining the h5 files, the h5py library can be used (where `my_database` is the location of the h5 files on your local filesystem)::

	import glob 
	import h5py
	myfiles = glob.glob(my_database+'*.h5')
	myh5 = h5py.File(myfile[0],"r")
	
	
Examining tree structure::

    print(myh5['equilibrium'].keys())
    print(myh5['equilibrium']['time_slice'].keys())
    print(myh5['equilibrium']['time_slice']['0'].keys())
    print(myh5['equilibrium']['time_slice']['0']['global_quantities'].keys())
    
    
which will return something like (not guaranteed to be up-to-date)::
    
    <KeysViewHDF5 ['code', 'ids_properties', 'time', 'time_slice', 'vacuum_toroidal_field']>
    <KeysViewHDF5 ['0', '1', '10', '100', '101', '102', '103']>
    <KeysViewHDF5 ['boundary', 'constraints', 'global_quantities', 'profiles_1d', 'profiles_2d', 'time']>
    <KeysViewHDF5 ['beta_normal', 'beta_tor', 'ip', 'li_3', 'magnetic_axis', 'psi_axis', 'psi_boundary', 'q_95', 'q_axis', 'q_min']>

	
Plotting a time-trace for a single shot::	
	
    import matplotlib as plt
    import numpy as np
    ip = []
    times = [] 
    betan = []
    for itime in myh5['equilibrium']['time_slice']:
        times.append(np.array(myh5['equilibrium']['time_slice'][itime]['time']['data'])
        ip.append(np.array(myh5['equilibrium']['time_slice'][itime]['global_quantities']['ip']))
        betan.append(np.array(myh5['equilibrium']['time_slice'][itime]['global_quantities']['beta_normal']))
    	
    plot(times, ip*1e-6, '.')
    plot(times, betan*1e-6, '.')

