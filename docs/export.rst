
Exporting data
==============

While the IMAS format is nicely standardized, no data format can satisfy all
needs and often one needs to export the data to an intermediate format.  In
this view, we distinguish export from extraction by the notion that export
scripts are meant for producing other files while extraction is there to
produce data for analysis. This is an arbitrary line, but is meant to give
an indication of the role.


Exporting metadata to pandas
-----------------------------

Key to making the EFIT-AI database FAIR is the notion of Findability.
The findability means that each key piece of data has a unique identifier
and enough metadata to find one based on specific criterion.   

To meet this criterion, we use pandas to create a table of the metadata 
for each directory, and then propogate up the directory tree.   Because 
our primary goal is to have a database of **equilibria**, the most important
unique identifier is the that of the equilibria itself.  We also note that
a time in a single pulse (shot), can have multiple equilibria due to resolutions
or equilibrium reconstruction type.  To address this, we use a hash of the
psi function itself.  This is a label that is extremely sensitive to any 
changes in psi -- subtle changes in inputs will give different round-off 
errors and hence a different hash.  But for database purposes where uniqueness
is the most important feature, this is the best label.

The metadata itself is the standard global parameters (beta normal, qmin, ...),
as well as additional shape parameters that exist in IMAS.  Here, we use
demonstrate how to export the metadata.   Using the metadata is straightforward
using pandas tools.


To do a quick test of the export to pandas::

   export_pandas.py -o pd.h5 178339.h5

We note that this pandas h5 file is not an IMAS formatted file.   Pandas also supports
other formats for storing the pandas dataframe including sqlite3.

Exporting data for EFIT-MOR
-------------------------------------------------------

**NOTE:  This is out of data and needs to be updated**

The EFIT-AI team developed a script to extract the necessary data from the IMAS database (DB). 
The said script, called `NN_io_fromMagDB.py`, contains a series of modules that operate on either
each time slice or discharge file from the DB (i.e. the .h5 files contained therein). 

For any discharge contained in the 2019 EFIT01 or EFIT02 DB, the IO vector for the EFIT-MOR 
can be built via the following Pyhton commands::

     from NN_io_fromMagDB import extract_inputs, extract_outputs
     hin = h5py.File(input_file, mode="r")
     inputs, times = extract_inputs(input_file, '01')
     EFIToutputs = extract_outputs(input_file)

where the flag 01 in `extract_inputs()` indicates that data is being pulled from EFIT01 (magnetics 
only). The user must switch this flag to 02 to pull the MSE data along with the magnetics from
EFIT02. This way of doing things will be phased out eventually. 

EFIToutputs is a list that contains 4 3D arrays: the normalized poloidal flux, Btor, 
JtorDS ( curl curl of psi) and JtorFB (= R*p' + FF'/(mu0*R) on the RZ grid as a function of the 
time slices contained within the discharge).

The inputs as a function of time and one of the 4 outputs as a function of the RZ grid can be
plotted via::

    plot_data(inputs, EFIToutputs, times, R.T, Z.T)

where the RZ grid coords can be obtained with::

    R, Z  = get_grid(time_slice)

after invoking `from NN_io_fromMagDB import get_grid`. 
The time-slice for the 2D contour plot of the EFIToutput (be it psi or Btor or Jtor etc) is 
chosen randomly over the available time slices encoded in the 1D vector times. 

Time slices with BetaN>10.0 are automatically discarded as bad data inside
the functions extract_inputs() amd extract_outputs()

Building the IO vectors for a series of discharges
++++++++++++++++++++++++++++++++++++++++++++++++++

An additional parallel Python script, save_efitmor_inputs.py, has been created to loop over a 
series of discharge files and monotonically build an input and output vector for the ML training. 
Because a small subset of the discharges are either a vacuum or a test shot, the user must invoke
the function find_goodShots(discharge_files), which checks the list of .h5 files, discharge_files, for
both vacuum and test shots, and only keeps non-vacuum and non-test shots. 

.. Warning::
     The user has to be sourcing the text file GoodShots.txt or GoodShots2019.txt 
     for the function find_goodShots() to sift through the discharges correctly.  
    
The code snippet to prune out the bad times is:: 

    # determine the "good" shots
    num_good_shots, goodID = findGoodShots(discharge_files)

The main routine is as follows::
    
    # // process the data 
    numthreads = mp.cpu_count()
    pool = mp.Pool(processes=numthreads)
    results = pool.map( pull_data, [infile for infile in discharge_files[goodID]])
    pool.close()
    pool.join()

    # parse the results into the separate arrays to create the IO of ML
    for ii in np.arange(num_good_shots):
        if ii == 0:
            inputs = results[ii][0]
            times = results[ii][1]
            psi = results[ii][2]
            Jtor = results[ii][3]
            JtorDS = results[ii][4]
        else:
            inputs = np.append(inputs, results[ii][0], axis=1)  
            times = np.append(times, results[ii][1])  
            psi = np.append(psi, results[ii][2], axis=2)  
            Jtor = np.append(Jtor, results[ii][3], axis=2)  
            JtorDS = np.append(JtorDS, results[ii][4], axis=2)  
 
