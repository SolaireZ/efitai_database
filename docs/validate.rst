
Validation of Data
===================

Python files relevant to this section:

  1. Command-line applications:

    + validator.py 
    + fix_dbfile.py 

  2. Utility modules:

    + validate_fields.py 
    + validate_time.py 
    + eqdb_utils.py 

Overview of Validation
----------------------

We define 3 validation levels:

  Level 0: Is the data there?
  Level 1: Is the data itself valid?
  Level 2: Is the data in a format that meets the needs of my specific workflow?

For IMAS, the standard specifies the format of the data if it is there, 
but does not mandate that all of the data be there.   This is in keeping
with a data format that is meant to be a standard for the entire fusion 
community.    For our needs, it is important that the equilibrium data
be there (or diagnostic data for those files), so the first check is 
merely if the data exists.

There are multiple reasons why the data might not be there: 1) the tool to 
automate the generation of it failed in some way, 2) the data transfer mechanism 
failed but it is not obvious that the data is corrupted, or 3) human error.  
Thus, a simple check if the data exists.  

The next check is on the quality of the data:  specifically if the data
passes various equilibrium reconstruction checks.   Usually this is due
to EFIT failing or having too loose of a tolerance, but the workflow tool 
did not catch it.  

Finally, the EFIT-AI database was created for machine learning.   Various 
information or labels are useful for that purpose.  Because IMAS, in its 
strictest mode does not allow additional data to be added, we use the 
`code/parameters` field for this additional data.   See our paper for more
details.

Validating and Fixing Database Files
------------------------------------

We start with discussing Level 0 validation using `validator.py`:

    ➜ validator.py 179565.h5

    Level 0: File valid (for set base): 179565.h5


There are multiple sets of fields to validate the existence of, for more
details see `validate_fields.py`, but the `base` set is sufficent for 
most of the EFIT-AI database needs.  To see all of the field members 
searched for, one can pass the `-v` flag for the verbose listing.

To understand how to fix Level 0 errors, we are going to break this file by
removing common data, and then adding it back in.   Because many workflows are
at a given machine and label the files by shot (or pulse) number, they are
often missing from the file.  This violates the philosophy of the datafile
being completely self-describing:  we do not want key information to come 
from the directory structure or file name.  Our goal then is to remove the
machine name and pulse number, and then add it back in.

To remove these fields, we use `fix_dbfile.py`::

   ➜ fix_dbfile.py -r "machine pulse" 179565.h5

Again, one can use the `-v` flag to see more information.  Now, check too
see if this broke the validation::

    ➜ validator.py 179565.h5

    Level 0: File NOT valid: (for set base): 179565.h5
      Failures are:  /dataset_description/data_entry/machine /dataset_description/data_entry/pulse

    Failures found in these files: ['179565.h5']

The summary is because when running this script over an entire directory, you
often want to just know the files that failed.   Now, let's fix this file::

    ➜ fix_dbfile.py -a "machine,DIII-D pulse,179565"  179565.h5
    Adding missing fields:  /dataset_description/data_entry/machine with DIII-D
    Adding missing fields:  /dataset_description/data_entry/pulse with 179565

It should pass again::

    ➜ validator.py 179565.h5

    Level 0: File valid (for set base): 179565.h5

For Level 1 validation, we need to specify to do that validation::

    ➜ validator.py -l 1 179565.h5
    Level 1: 179565.h5, # of invalid times 4
    Failures found in these files: ['179565.h5']


Recall from `list_equilibria.py`, that this file had 13 equilibria in it.  To 
see which equilibria are bad, use the `-v` option::

    ➜ validator.py -v -l 1 179565.h5
    Initializing
    Processing  179565.h5
    This OMAS .h5 file was generated with EFIT01
    Level 1: 179565.h5, # of invalid times 4
    Failed slices:  [1, 10, 11, 12]
    Failures found in these files: ['179565.h5']

This is at the end of the equilibria so perhaps it disrupted or 
EFIT had difficulty reconstructing the shutdown phase.   One could
use the extract/plot scripts discussed later to examine in more 
detail and figure out the best way of fixing it.  Here, 
we use `fix_dbfile.py` to just remove the bad equilibria::

    ➜ fix_dbfile.py -v -S -t 179565.h5
    Initializing
    Fixing time slices
    Processing  179565.h5

The file is now valid::

    ➜ validator.py -l 1 179565.h5
    Level 1: 179565.h5 has all valid time slices.

The `-S` option above saved the original file into a "Save" 
subdirectory from wherever the script was invoked.   You can use 
`list_equilibria.py` on the original and modified file to explicitly 
show which time slices were removed.
