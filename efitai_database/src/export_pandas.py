#!/usr/bin/env python
"""
Export data to files
"""
import os
import glob
import pandas as pd
import extract_equilibrium as exeq


class EaPandas(exeq.Equilibria):
    def __init__(self, options=None):
        """
        Export pandas relies heavily on the extract_equilibrium
        functionality.  Basically, loop over all files in a directory
        """
        super().__init__(options=options)

        directory = options.files[0]
        if not os.path.exists(directory):
            print("Input file ", directory, " must exist.")
            return

        self.directory = directory

        # This is where the equilibrium dataframe is stored
        self.eqdf = pd.DataFrame()

        # We ignore most stuff in the OMAS tree
        self.excludes = "constraints code profiles_2d".split()

        self.process_directory()

    def process_directory(self):
        """
        This automatically loops over all files.  get_dataframe gets
        the data for each equilibrium time slice in that file and puts
        into the global dataframe.

        One of the issues here is that we want to store the location file
        within the larger EFIT database.  Punting for now.
        """
        for file in glob.glob(os.path.join(self.directory, "*.h5")):
            self.get_dataframe(file)

        return

    def get_dataframe(self, input_file):
        """
        The dictionary the comes from extract_equilibrium has too many nestings
        when what we want are uniq names for columns so we have to reformat
        that OMAS-oriented nested dictionary with a new one.

        General order of dict to be converted to dataframe is:
        machine - pulse - time - eq_label - <other quantities>
        """
        self.get_from_file(input_file)

        for shotlbl in self.equilibria:
            machine, shot = shotlbl.split("_")
            machine = str(machine)
            shot = int(shot)
            for time in self.equilibria[shotlbl]:
                for eqrun in self.equilibria[shotlbl][time]:
                    d = self.equilibria[shotlbl][time][eqrun]
                    e = self._fill_dict(d)

                    # Basic metadata
                    # TODO:  Need eqtype
                    # e["eqlabel"] = eqrun  # Use as index
                    e["filename"] = input_file
                    e["machine"] = machine
                    e["pulse"] = int(shot)
                    e["time"] = float(time)

                    # TODO:  It would be more performant to make a column of
                    # series per file
                    if self.eqdf.empty:
                        self.eqdf = pd.DataFrame(e, index=[eqrun])
                    else:
                        self.eqdf = pd.concat(
                            [self.eqdf, pd.DataFrame(e, index=[eqrun])]
                        )
        #
        # Returning the df itself is useful for interactive work
        return self.eqdf

    def _fill_dict(self, d):
        """
        Fill the pandas-style dictionary (e)  based on the input dictionary (d)
        """
        e = {}

        e["xpt0_r"] = d["boundary"]["x_point"]["0"]["r"][()]
        e["xpt0_z"] = d["boundary"]["x_point"]["0"]["z"][()]
        e["xpt1_r"] = d["boundary"]["x_point"]["1"]["r"][()]
        e["xpt1_z"] = d["boundary"]["x_point"]["1"]["z"][()]

        g = d["global_quantities"]
        e["beta_normal"] = g["beta_normal"][()]
        e["beta_tor"] = g["beta_tor"][()]
        e["ip"] = g["ip"][()]
        e["li_3"] = g["li_3"][()]
        e["magnetic_axis.b_field_tor"] = g["magnetic_axis"]["b_field_tor"][()]
        e["magnetic_axis.r"] = g["magnetic_axis"]["r"][()]
        e["magnetic_axis.z"] = g["magnetic_axis"]["z"][()]
        e["psi_axis"] = g["psi_axis"][()]
        e["psi_boundary"] = g["psi_boundary"][()]
        e["q_axis"] = g["q_axis"][()]
        e["q_95"] = g["q_95"][()]
        e["q_min"] = g["q_min"]["value"][()]
        e["q_min.rho_tor_norm"] = g["q_min"]["rho_tor_norm"][()]

        p = d["profiles_1d"]
        e["elongation"] = p["elongation"][()][-1]
        e["triangularity_upper"] = p["triangularity_upper"][()][-1]
        e["triangularity_lower"] = p["triangularity_lower"][()][-1]
        e["volume"] = p["volume"][()][-1]
        e["area"] = p["area"][()][-1]
        e["surface"] = p["surface"][()][-1]

        # Floats are sufficient for metadata and take up less space
        for qty in e:
            e[qty] = float(e[qty])

        return e

    def restore(self, input_file):
        """
        Restore a previously save eq dataframe
        """
        return
        # TODO: Pandas changed interface and now have to update
        # with pd.HDFStore(input_file) as store:
        #     self.eqdf = pd.concat(self.eqdf, store["efitai"])

    def dump(self, output_file):
        """
        Write the data -- no sanity checks.
        This overwrites the normal h5py dump in extract.py because we just use
        pandas natural functionality

        TODO:  Add metadata about the metadata (clunky in h5py)
        In particular, we want something about the directory structure, date
        created, etc.
        """
        store = pd.HDFStore(output_file)
        store.put("efitai", self.eqdf)
        store.close()


def main():
    """
    Convert
    """
    desc = "Directory containing EFIT-AI database files. Only 1 dir at a time"
    parser = exeq.parse_eqargs(description=desc)
    parser.add_argument(
        "-i",
        "--input",
        help="Add dataframe in stored HDF5 file to dataframe",
        default=None,
    )
    args = parser.parse_args()

    # Constructor does all the work
    eadf = EaPandas(args)

    # Add saved file to dataframe (if just a restore, one must give a directory
    # with no output files
    if args.input:
        eadf.restore(args.input)

    if args.output_file:
        eadf.dump(args.output_file)


if __name__ == "__main__":
    main()
