#!/usr/bin/env python
"""
Base class for all extract_ classes
"""
import os
import glob
import argparse
import h5py
import numpy as np
import eqdb_utils


class ExtractData:
    def __init__(self, options=None):
        if options:
            self.verbose = options.verbose
        else:
            self.verbose = False

        self._data = {}
        self._stored_data = {}
        # This should be defined for each derived class
        # See extract_equilibrium for an example of
        # implementation expectations.
        self.root_name = None
        # TODO:  Show the property trick here

    def _get_shotlabel_from_h5(self, h5in):
        """
        Find the machine and shot to label the root directory
        """
        metagrp = h5in.get("dataset_description/data_entry")
        machine = metagrp["machine"].asstr()[...]
        # Pulse sometimes seems to be a string and somtimes an integer
        try:
            pulse = metagrp["pulse"].asstr()[...]
        except TypeError:
            pulse = str(metagrp["pulse"][()])
        return machine + "_" + pulse

    def _store_shotlbl(self, shotlbl):
        """
        Keep track of the shotlabels loaded into data.  Generally used just by
        extract_equilibrium since we need to know what equilibria is available
        """
        if shotlbl not in self._stored_data:
            self._stored_data[shotlbl] = []
        return

    def _store_time(self, shotlbl, time):
        """
        Keep track of the time loaded into data.  Generally used just by
        extract_equilibrium since we need to know what equilibria is available
        """
        if time not in self._stored_data[shotlbl]:
            self._stored_data[shotlbl].append(time)
        return

    def _get_shotlabel(self):
        """
        Return all of the stored shotlbls as a list
        """
        return list(self._stored_data.keys())

    def _get_time(self, shotlbl):
        """
        Return the time as a numpy array
        """
        return np.array(self._stored_data[shotlbl], dtype=np.float64)

    def get_from_file(self, input_file, h5in=None):
        """
        This method should be defined for each derived class
        #TODO:   Make this a generic get everything
        """
        print("Method not defined")
        return

    def get_wall_from_file(self, input_file, shotlabel, h5in=None):
        """
        Get the wall info which is very useful when plotting
        """
        if (h5in is None):
            h5in = h5py.File(input_file, mode="r")

        rb = np.array(h5in['/wall/description_2d/0/limiter/unit/0/outline/r'])
        zb = np.array(h5in['/wall/description_2d/0/limiter/unit/0/outline/z'])
        self._data["equilibria"][shotlabel]['wall'] = [rb, zb]

        return

    def get_from_directory(self, directory):
        """
        Give a directory and loop over all of the files in it
        """
        for file in glob.glob(os.path.join(directory, "*.h5")):
            self.get_from_file(file)
        return

    def dump(self, outfile):
        """
        Save the nested dictionary  into an h5file
        """
        file_exists = os.path.exists(outfile)
        if file_exists:
            print("Adding to file: ", outfile)
            hout = h5py.File(outfile, mode="r+")
        else:
            print("Creating new file: ", outfile)
            hout = h5py.File(outfile, mode="w")

        # Now write out the nested dictionary
        for key, value, h5node in eqdb_utils.write_dict(self._data[self.root_name], hout):
            # print(key)
            if key not in h5node:
                h5node.create_dataset(key, data=value)

        return

    def restore(self, restore_file):
        """
        Save the data dictionary into an h5file
        """
        file_exists = os.path.exists(restore_file)
        if file_exists:
            print("Restoring from file: ", restore_file)
            h5in = h5py.File(restore_file, mode="r")
        else:
            print("Cannot fine restore file: ", restore_file)
            return

        #  Get the route
        eqdb_utils.datalist = []
        self._data[self.root_name] = eqdb_utils.getdbvals(h5in)

        return


def parse_extract_args(description=None):
    """
    General arguments that control the extraction
    """
    if description:
        desc=description
    else:
        desc = "Program for extracting data from an EFIT-AI Database file"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('files', type=str, nargs='+',
                        help='EFIT-AI Database files')
    parser.add_argument(
        "-r",
        "--restore",
        help="Restore data from a previous write(dump) iile",
        dest="restore_file",
        default=None
    )
    parser.add_argument(
        "-e",
        "--excludes",
        help="Comma-delimited list of strings to exclude in ",
        dest="excludes",
        default=None
    )
    parser.add_argument(
        "-o",
        "--output",
        help="Write data to output file",
        dest="output_file",
        default=None
    )
    parser.add_argument(
        "-v", "--verbose",
        help="Verbose output",
        dest="verbose",
        action="store_true"
    )
    # I do not understand this, but when running in a jupyter notebook, this is
    # required.  Not needed from CLI.
    parser.add_argument(
        "-F",
        "--foo",
        help="Try to debug optparse",
        dest="foo",
        default=None
    )
    return parser
