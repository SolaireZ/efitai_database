#!/usr/bin/env python
"""
Extract the equilibrium from an OMAS-formatted file
"""
import os
import h5py
import numpy as np
import eqdb_utils
import zlib
from extract import ExtractData, parse_extract_args


class Equilibria(ExtractData):
    def __init__(self, options=None):
        super().__init__(options=options)

        self.time_in = None
        self.slice_in = None
        self.excludes = []
        self.n_eq = 0

        if options:
            if options.time:
                self.time_in = options.time
            if options.slice:
                self.slice_in = options.slice
            if options.excludes:
                self.excludes = options.excludes.split(",")

        # For convenience, make this the same as the key name in the
        # @property function below
        self.root_name = "equilibria"
        self._data[self.root_name] = {}

    # Useful extract function/decorator:
    # Allows the referral to self.equilibria as a member
    @property
    def equilibria(self):
        return self._data["equilibria"]

    def get_from_file(self, input_file, h5in=None):
        """
        Simple wrapper for get_eq_from_file -- get_eq_from_file is reused
        by sub-classes while get_from_file is the base method to overwrite
        """
        # Get handles
        if not h5in:
            h5in = h5py.File(input_file, mode="r")
        self.get_eq_from_file(h5in)
        return

    def get_eq_from_file(self, h5in):
        """
        Basic datastructure to fill:
          self.equilibria[shotlbl][timenm][eqlbl]  = OMAS-like dictionary

        This allows multiple equilibria (from different files)
        """
        shotlbl = self._get_shotlabel_from_h5(h5in)
        if not shotlbl:
            print("File invalid because cannot find machine or shot number.")
            return

        # Open the groups of interest
        # TODO:  Need to get the vacuum toroidal fields
        eqgrp = h5in.get("equilibrium")
        time = eqgrp.get("time")[()]

        slice_select = self._get_slices(time)

        eqdct = {}
        for nts in slice_select:
            nts = int(nts)
            timegrp = eqgrp.get("time_slice/" + str(nts))
            timenm = str(time[nts])
            eqdct[timenm] = {}
            eqlbl = self._get_eqlabel(timegrp)
            eqdb_utils.datalist = []
            if self.verbose:
                print(timenm, "\n    ", eqlbl)
            eqdct[timenm][eqlbl] = eqdb_utils.getdbvals(timegrp, self.excludes,
                                                        self.verbose)
            self.n_eq += 1

        if shotlbl not in self.equilibria:
            self.equilibria[shotlbl] = {}
        self._store_shotlbl(shotlbl)

        # time slice could already exist from combining multiple files
        for timenm in eqdct:
            if timenm not in self.equilibria[shotlbl]:
                self.equilibria[shotlbl][timenm] = eqdct[timenm]
                self._store_time(shotlbl, timenm)
            else:
                # Assume one eqlbl per timenm
                eqlbl = list(eqdct[timenm].keys())[0]
                self.equilibria[shotlbl][timenm][eqlbl] = eqdct[timenm][eqlbl]

        self.get_wall_from_file(None, shotlbl, h5in=h5in)

        return

    def _get_eqlabel(self, tgrp):
        """
        The efitai "database" can/should have multiple equilibria for a single
        discharge/time slice.   This requires some type of labeling for each
        equilibria.  Rather than try to come up with some type of complicated
        naming scheme (from files which are usually incomplete in their
        provenance, we use a reproducible hash of the flux function to label
        """
        psi2d = tgrp.get("profiles_2d/0/psi")  # Why the 0?
        # Use adler32 because that is sufficient for our purposes and gives
        # short hashes
        return "eq" + str(zlib.adler32(psi2d[()].data.tobytes()))

    def _get_slices(self, time):
        """
        Choose the slices from OMAS file to add to datastructure based on
        either the time or slice inputs
        """
        t_str = ",".join(str(x) for x in time)
        timestr = t_str.split(",")
        slice_select = None
        if self.time_in:
            # Select times from input
            time_select = self.time_in.split(",")
            slice_select = []
            for ts in time_select:
                if ts in timestr:
                    slice_select.append(timestr.index(ts))
                else:
                    print("Time ", ts, " not found.")

        if self.slice_in:
            slice_select = self.slice_in.split(",")

        if not slice_select:
            slice_select = np.arange(time.size)

        return slice_select

    # TODO:  Convert to using our datastructure
    def getPsi(self, eq, r, z):
        from scipy import interpolate

        self.r0 = eq["global_quantities"]["magnetic_axis"]["r"][()]
        self.z0 = eq["global_quantities"]["magnetic_axis"]["z"][()]
        self.psi_axis = eq["global_quantities"]["psi_axis"][()]
        self.psi_boundary = eq["global_quantities"]["psi_boundary"][()]

        psi = interpolate.interp2d(
            eq["profiles_2d"]["0"]["grid"]["dim1"],
            eq["profiles_2d"]["0"]["grid"]["dim2"],
            eq["profiles_2d"]["0"]["psi"].T,
            kind="linear",
        )(r, z)[0]
        psi_norm = (psi - self.psi_axis) / (self.psi_boundary - self.psi_axis)
        return psi_norm, psi

    # TODO:  This should be a method outside of the class, and then we need a
    # class method that just gives the times that are in the structure.
    def get_times(self, shotlbl):
        """
        Returns the times given a shotlabel
        """
        timekeys = list(self.equilibria[shotlbl].keys())
        if 'wall' in timekeys:
            timekeys.remove('wall')
        return timekeys


def parse_eqargs(description=None):
    """
    Routine for getting the arguments for extracting equilibrium.py
    It is it's own routine to allow other scripts (like plot_equilibrium.py)
    to use it.
    """
    parser = parse_extract_args(description)
    parser.add_argument(
        "-s",
        "--slice",
        help="Comma delimited list of time slices (integers) select",
        default=None,
    )
    parser.add_argument(
        "-t", "--time", help="Comma delimited list of times select",
        default=None
    )
    parser.add_argument(
        "-l",
        "--list",
        help="List all of the times that have equilibria",
        dest="list",
        action="store_true",
    )
    return parser


def main():
    """
    Parse arguments and act accordingly
    """
    parser = parse_eqargs()
    args = parser.parse_args()

    # No matter what, this only processes a single file at a time
    input_file = args.files[0]

    if not os.path.exists(input_file):
        print("Input file ", input_file, " must exist.")
        return
    if os.path.splitext(input_file)[1] not in [".h5"]:
        print("Input file ", input_file, " must be an h5 file.")
        return

    # Sometimes it's useful just to see the times
    if args.list:
        if args.verbose:
            print("LISTING FILE")
        eqdb_utils.list_times_in_file(input_file)
        return

    # Show how class is used
    if args.verbose:
        print("INSTANTIATING CLASS")

    eqs = Equilibria(args)
    if args.restore_file:
        eqs.restore(args.restore_file)

    eqs.get_from_file(input_file)

    if args.output_file:
        eqs.dump(args.output_file)


if __name__ == "__main__":
    main()
