from omas import *
import pandas
import glob
import h5py
import numpy

db = {
    "machine": [],
    "pulse": [],
    "time": [],
    "beta_normal": [],
    "beta_pol": [],
    "beta_tor": [],
    "ip": [],
    "li_3": [],
    "magnetic_axis.b_field_tor": [],
    "magnetic_axis.r": [],
    "magnetic_axis.z": [],
    "psi_axis": [],
    "psi_boundary": [],
    "q_axis": [],
    "q_min.rho_tor_norm": [],
    "q_min.value": [],
    "q_95": [],
    "elongation": [],
    "triangularity_lower": [],
    "triangularity_upper": [],
    "volume": [],
    #"area": [],
    #"surface": [],
}


db_dir = ""

fits = glob.glob(db_dir + "*.h5")
for fit in fits:
    ods = ODS()
    ods = h5py.File(fit, "r")
    # ods.load(fit,consistency_check=False)
    print(fit)
    try:
        for itime in ods["equilibrium"]["time_slice"]:
            db["machine"].append("DIII-D")
            db["pulse"].append(
                float(numpy.array(ods["dataset_description"]["data_entry"]["pulse"]))
            )
            db["time"].append(
                float(numpy.array(ods["equilibrium"]["time_slice"][itime]["time"]))
            )
            db["beta_normal"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["global_quantities"][
                            "beta_normal"
                        ]
                    )
                )
            )
            db["beta_pol"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["global_quantities"][
                            "beta_pol"
                        ]
                    )
                )
            )
            db["beta_tor"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["global_quantities"][
                            "beta_tor"
                        ]
                    )
                )
            )
            db["ip"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["global_quantities"][
                            "ip"
                        ]
                    )
                )
            )
            db["magnetic_axis.b_field_tor"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["global_quantities"][
                            "magnetic_axis"
                        ]["b_field_tor"]
                    )
                )
            )
            db["magnetic_axis.r"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["global_quantities"][
                            "magnetic_axis"
                        ]["r"]
                    )
                )
            )
            db["magnetic_axis.z"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["global_quantities"][
                            "magnetic_axis"
                        ]["z"]
                    )
                )
            )
            db["psi_axis"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["global_quantities"][
                            "psi_axis"
                        ]
                    )
                )
            )
            db["psi_boundary"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["global_quantities"][
                            "psi_boundary"
                        ]
                    )
                )
            )
            db["q_axis"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["global_quantities"][
                            "q_axis"
                        ]
                    )
                )
            )
            db["li_3"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["global_quantities"][
                            "li_3"
                        ]
                    )
                )
            )
            db["q_min.rho_tor_norm"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["global_quantities"][
                            "q_min"
                        ]["rho_tor_norm"]
                    )
                )
            )
            db["q_min.value"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["global_quantities"][
                            "q_min"
                        ]["value"]
                    )
                )
            )
            db["q_95"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["global_quantities"][
                            "q_95"
                        ]
                    )
                )
            )
            #db["elongation"].append(
            #    ods["equilibrium"]["time_slice"][itime]["profiles_1d"]["elongation"][-1]
            #)
            #db["triangularity_lower"].append(
            #    ods["equilibrium"]["time_slice"][itime]["profiles_1d"][
            #        "triangularity_lower"
            #    ][-1]
            #)
            #db["triangularity_upper"].append(
            #    ods["equilibrium"]["time_slice"][itime]["profiles_1d"][
            #        "triangularity_upper"
            #    ][-1]
            #)
            db["elongation"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["boundary_separatrix"][
                            "elongation"
                        ]
                    )
                )
            )
            db["triangularity_lower"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["boundary_separatrix"][
                            "triangularity_lower"
                        ]
                    )
                )
            )
            db["triangularity_upper"].append(
                float(
                    numpy.array(
                        ods["equilibrium"]["time_slice"][itime]["boundary_separatrix"][
                            "triangularity_upper"
                        ]
                    )
                )
            )
            db["volume"].append(
                ods["equilibrium"]["time_slice"][itime]["profiles_1d"]["volume"][-1]
            )
            #db["area"].append(
            #    ods["equilibrium"]["time_slice"][itime]["profiles_1d"]["area"][-1]
            #)
            #db["surface"].append(
            #    ods["equilibrium"]["time_slice"][itime]["profiles_1d"]["surface"][-1]
            #)
    except Exception:
        pass

    ods.close()

df = pandas.DataFrame(data=db)
df.to_csv("summary.csv")
