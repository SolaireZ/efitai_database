#!/usr/bin/env python
"""
Extract the equilibrium from an OMAS-formatted file
"""
import argparse
import os
import numpy as np
import eqdb_utils


def parse_listargs():
    """
    Routine for getting the arguments
    """
    desc = "List the equilibria times and count in the files given"
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument("files", type=str, nargs="+",
                        help="EFIT-AI Database files")

    parser.add_argument(
        "-v", "--verbose",
        help="Verbose output",
        dest="verbose",
        action="store_true"
    )
    parser.add_argument(
        "-c",
        "--count",
        help="Instead of listing, just count the equilibria",
        action="store_true",
    )
    return parser


def list_equilibria(args):
    """
    Main function of module
    """
    n_all_eq = 0
    for file in args.files:
        if args.verbose:
            print(file)
        if not os.path.exists(file):
            print("File does not exist", file)
            continue

        # This is where all the work really happens
        time = eqdb_utils.list_times_in_file(file, sprint=False)

        n_time = len(time)
        n_all_eq += n_time
        if args.count:
            print(file, "\t", n_time)
        else:
            print(os.path.basename(file))
            time_slices = np.arange(n_time)
            for ts in time_slices:
                print("\t", ts, "\t", str(time[ts])+" s")

    print(f"Total number of equilibria: {n_all_eq}")


def main():
    """
    Parse arguments and act accordingly
    """
    parser = parse_listargs()
    args = parser.parse_args()
    list_equilibria(args)


if __name__ == "__main__":
    main()
