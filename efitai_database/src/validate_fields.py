# For validator.py:
#     Use one of the sets listed in this section for level0 validation
#     Level 2 validation is equivalent to extensions

# Name of sets currently required in what is considered base files
base = ["required", "equilibria_required", "magnetics_required"]

# Additional requirements.  Diagnostics are now in separate files.
additional = ["diagnostics_required"]

small = ["required"]

# This is used by fix_dbfile.py to enable shortnames of
#  anything in this file
allsets = base + additional + ['extensions']

level2 = ['extensions']

# ------------------------------------------------------------
# Dataset/group sets
# The required data is the equilibrium data plus that used for
#   the indexing of the metadata
# See export_pandas.py for the fields used
#
required = []
required += ["/dataset_description/data_entry/machine"]
required += ["/dataset_description/data_entry/pulse"]
required += ["/equilibrium/time_slice/0/boundary"]
required += ["/equilibrium/time_slice/0/boundary/x_point/0/r"]
required += ["/equilibrium/time_slice/0/boundary/x_point/0/z"]
required += ["/equilibrium/time_slice/0/boundary/x_point/1/r"]
required += ["/equilibrium/time_slice/0/boundary/x_point/1/z"]
required += ["/equilibrium/time_slice/0/global_quantities"]
required += ["/equilibrium/time_slice/0/global_quantities/beta_normal"]
required += ["/equilibrium/time_slice/0/global_quantities/beta_normal"]
required += ["/equilibrium/time_slice/0/global_quantities/ip"]
required += ["/equilibrium/time_slice/0/global_quantities/li_3"]
required += ["/equilibrium/time_slice/0/global_quantities/magnetic_axis"]
required += ["/equilibrium/time_slice/0/global_quantities/psi_axis"]
required += ["/equilibrium/time_slice/0/global_quantities/psi_boundary"]
required += ["/equilibrium/time_slice/0/global_quantities/q_axis"]
required += ["/equilibrium/time_slice/0/global_quantities/q_95"]
required += ["/equilibrium/time_slice/0/global_quantities/q_min"]
required += ["/equilibrium/time_slice/0/profiles_1d"]
required += ["/equilibrium/time_slice/0/profiles_1d/elongation"]
required += ["/equilibrium/time_slice/0/profiles_1d/triangularity_upper"]
required += ["/equilibrium/time_slice/0/profiles_1d/triangularity_lower"]
required += ["/equilibrium/time_slice/0/profiles_1d/volume"]
required += ["/equilibrium/time_slice/0/profiles_1d/area"]
required += ["/equilibrium/time_slice/0/profiles_1d/surface"]

equilibria_required = []
equilibria_required += ["/equilibrium/code/parameters"]
equilibria_required += ["/equilibrium/time"]
equilibria_required += ["/equilibrium/time_slice"]
equilibria_required += ["/equilibrium/vacuum_toroidal_field"]

magnetics_required = []
eq0 = "/equilibrium/time_slice/0"   # Looking at just first time slice
magnetics_required += [eq0+"/constraints/bpol_probe"]
magnetics_required += [eq0+"/constraints/flux_loop"]
magnetics_required += [eq0+"/constraints/ip"]
magnetics_required += [eq0+"/constraints/diamagnetic_flux"]
magnetics_required += [eq0+"/constraints/pf_current"]

kinetics_required = []
kinetics_required += ["/wall"]
kinetics_required += ["/core_profiles", "/core_sources", "edge_profiles"]

diagnostics_required = ["/charge_exchange"]
diagnostics_required += ["/interferometer"]
diagnostics_required += ["/thomson_scattering"]

# ------------------------------------------------------------
#  Level 2 validation: Useful but not part of IMAS

extension_loc = "/equilibrium/code/parameters"
extensions = []
extensions += [extension_loc + "/eqtype"]
extensions += [extension_loc + "/pulse_year"]
# extensions += [extension_loc + "/pulse_month"]
# extensions += [extension_loc + "/pulse_day"]
# extensions += [extension_loc + "/pulse_label_1"]
# extensions += [extension_loc + "/pulse_label_2"]
# extensions += [extension_loc + "/scenario_label0"]
# # startup, flattop, shutdown, disruption
# extensions += [extension_loc + "/pulse_time_label"]


def validate_list(validate_set):
    """
    Return a single list of all things to check given the set names
    shown at top of file
    """
    long_list = []
    vset_list = eval(validate_set)
    for vlistname in vset_list:
        long_list += eval(vlistname)

    return long_list
