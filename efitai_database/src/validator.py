#!/usr/bin/env python
"""
File to validate h5 files to see if the conform to various EFIT-AI standards
"""
import argparse
import h5py
import os
import shutil
import numpy as np
import validate_fields as vf
import validate_time as vt


class Validate:
    def __init__(self, options):
        """
        Instantiate class based on options
        """
        if options.verbose:
            print("Initializing")
        self.verbose = options.verbose
        self.files = options.files
        self.validate_set = options.validate_set
        self.save_files = options.save_files
        if options.validate_levels == 'a':
            options.validate_levels = '0,1,2'
        self.validate_levels = options.validate_levels.split(',')

    def validate_files(self):
        """
        Validate the entire file
        """
        l0fail = l1fail = l2fail = False
        failures = []
        # For performance, outer loop is loop over files to minimize
        #  the h5py opens, which can be expensive
        for file in self.files:
            basefile = os.path.basename(file)
            if not os.path.exists(file):
                print("File does not exist", file)
                continue

            h5in = h5py.File(file, mode="r")

            if self.verbose:
                print("Processing ", file)

            if '0' in self.validate_levels:
                l0fail = self._level0_validation(h5in, file)

            if '1' in self.validate_levels:
                l1fail = self._level1_validation(h5in, file)

            if '2' in self.validate_levels:
                l2fail = self._level2_validation(h5in, file)

            h5in.close()

            if l0fail or l1fail or l2fail:
                failures.append(basefile)

        if failures:
            print(f"Failures found in these files: {failures}")
        else:
            print(f"Success!  No failures found.")

        return

    def _level0_validation(self, hin, file):
        """
        Level 0 validation is whether things exist or not
        """
        ind = "  "
        member_fail = []
        basefile = os.path.basename(file)
        for member in vf.validate_list(self.validate_set):
            if member not in hin:
                if self.verbose:
                    print(3 * ind, "Member not found ", member)
                member_fail.append(member)
            else:
                if self.verbose:
                    print(3 * ind, "Member found ", member)
        tstr = "(for set " + self.validate_set + "): " + basefile
        if member_fail:
            print("\nLevel 0: File NOT valid: " + tstr)
            print("  Failures are: ", " ".join(member_fail), "\n")
            if self.save_files:
                if not os.path.exists("Save"):
                    os.mkdir("Save")
                shutil.move(file, "Save")
            return True
        else:
            print("\nLevel 0: File valid " + tstr)
            return False

    def _level1_validation(self, hin, file):
        """
        Level 1 validation is about passing equilibrium checks
        (see validate_time.py)
        """
        v = self.verbose
        basefile = os.path.basename(file)
        valid_slices, n_slices = vt.get_valid_timeslices(hin, verbose=v)
        n_valid_slices = len(valid_slices)
        n_invalid_slices = n_slices - n_valid_slices
        if n_invalid_slices > 0:
            print(f"Level 1: {basefile}, # of invalid times {n_invalid_slices}")
            if self.verbose:
                fail_slices = list(set(np.arange(n_slices))-set(valid_slices))
                print(f"Failed slices:  {fail_slices}")
            return True
        else:
            print(f"Level 1: {basefile} has all valid time slices.")
            return False

    def _level2_validation(self, hin, file):
        """
        Level 2 validation is additional stuff that is nice to
        have, but not strictly necessary
        Could possibly generalize level0 here but keeping separate
        because we do not understand future requirements
        """
        ind = "  "
        member_fail = []
        for member in vf.validate_list('level2'):
            if member not in hin:
                if self.verbose:
                    print(3 * ind, "Member not found ", member)
                member_fail.append(member)
            else:
                if self.verbose:
                    print(3 * ind, "Member found ", member)
        tstr = "(for set " + self.validate_set + "): " + file
        if member_fail:
            print("Level 2: File NOT valid: " + tstr)
            print("  Failures are: ", " ".join(member_fail), "\n")
            return True
        else:
            print("Level 2: File valid " + tstr)
            return False


def parse_valargs():
    """
    Routine for parsing arguments
    """
    desc = "Validate an IMAS file for EFIT-AI database"
    parser = argparse.ArgumentParser(description=desc)

    # All of these should mostly be the same fix_dbfile.py to simplify usage

    parser.add_argument("files", type=str, nargs="+",
                        help="EFIT-AI Database files")

    rh = "Save (via move) invalid files to subdirectory (Level 0 only)"
    parser.add_argument("-S", "--save_files", action="store_true", help=rh)

    parser.add_argument("-v", "--verbose", help="Verbose output",
                        action="store_true")

    rh = "Set name to use in Level 0 validating (see validate_fields.py)"
    parser.add_argument("-s", "--validate_set", help=rh, default="base")

    rh = "Comma delimited list of validation levels to perform (0,1,2,a)"
    parser.add_argument("-l", "--validate_levels", help=rh, default='0')

    return parser


def main():
    """
    Parse arguments and options and act accordingly
    """
    parser = parse_valargs()
    args = parser.parse_args()

    # Main work
    val = Validate(args)
    val.validate_files()


if __name__ == "__main__":
    main()
