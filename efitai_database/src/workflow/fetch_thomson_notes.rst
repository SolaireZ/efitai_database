

Desired interface
==================

`fetch_thomson.py */*.h5`

Pseudocode
===========
    for file in h5_files:
        machine, shot = get_machine_shot(file)
        get_thomson_data_from_mdsplus()
        write_thomson_data(file)

