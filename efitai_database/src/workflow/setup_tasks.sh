#!/usr/bin/env bash

# clean-up any unwanted, failed, or conflicting files
[ -f tasks.txt ] && rm tasks.txt
[ -f tasks.txt.tfin ] && rm *tasks.txt.tfin
files=`ls $1 | grep .h5 | wc -l`
[ $files -gt 0 ] && rm $1/*.h5
[ -f failed.txt ] && rm failed.txt
[ -f $1/summary.csv ] && rm $1/summary.csv
[ -f make_summary.sh ] && rm make_summary.sh

# save location of taskfarmer files
iwd=`pwd`

# make list of shots
shots=`ls $1`

# form task file
echo "#!/usr/bin/env bash" > tasks.txt
echo "" >> tasks.txt
for shot in $shots; do
  echo $iwd/wrapper.sh $1 $shot >> tasks.txt
done

echo "#!/usr/bin/env bash" > make_summary.sh
echo "" >> make_summary.sh
echo $iwd/wrap_summary.sh $1 >> make_summary.sh
chmod 777 make_summary.sh
