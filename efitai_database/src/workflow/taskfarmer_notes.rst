Purpose
-------

Taskfarmer is a workflow manager developed by NERSC to coordinate execution of multiple
separate processes within a single job.  This makes it very useful for database creation,
where the number of shots and time it takes each to run can be highly variable.  It also
makes it easy to chain multiple steps together, like running EFIT and then a standalone
python script.

For more background and the limitations of taskfarmer see the `NERSC documentation <https://docs.nersc.gov/jobs/workflow/taskfarmer/>`__.

The scripts provided here are designed to run EFIT with all of the database checks activated
and output to an HDF5 file with the OMAS implementation of IMAS format for each shot.

To perform this efficiently, each shot is treated as a separate task and farmed out to
compute nodes.  The scripts are setup for using perlmutter (128 cores per node), but can easily
be ported to a different system (or to meet the users' preference). Taskfarmer occupies a full
node to manage the tasks, so the job allocation must include at least 2 nodes.  More nodes can
be used as workers if the job allocation size (specified by `-N`) is increased.

Note: as of June 2023 there appears to be some problems with taskfarmer using more than 2 nodes.
One problem is that it does not seem to to communicate the restart information so jobs end up
starting over.  A second issue is that some tasks are left unfinished.  These can be avoided
by always using limiting restarts to 2 nodes and submitting an extra restart after the job
appears to have completed successfully.

Requirements
------------

This workflow assumes that k-files which will be used to produce the data are already
available.  The process of assembling all the k-files varies by how the experimental
data servers are setup, so this is left for users to work out ahead of time.

Furthermore, it is assumed that the k-files are separated into different directories
based on the shot numbers and that the name of each of these directories is the shot
number (and nothing else).  The parent directory containing subdirectories for each shot
should not contain any other files or directories.  This is where the OMAS HDF5 files 
will be saved as well.  

This configuration is based on convenience and the scripts can be modified to accomadate
a different arrangment if needed.  Users will need to be familiar with bash and python
scripting and read through all scripts executed in order to modify the directory
configuration to their liking (not recommended).

When adding a new experiment (besides DIII-D) the corresponding support files will need to
be setup and linked (in line 2 of `efit.input`).  An important part of what is needed, and
likely does not exist for a new experiment, are the bounds used for validating solutions.
This includes the following variables, which must be added to the `MACHINEIN` namelist in
the relevant `dprobe.dat` support file::

        Plasma inductance (`li_min` < L_I < `li_max`)
        Poloidal beta (0 < beta_pol < `betap_max`%)
        Toroidal beta (0 < beta_tor < `betat_max`%)
        Total plasma current matches constraint to within `plasma_diff`
        Plasma shape (`aminor_min` cm < minor radius < `aminor_max` cm, `elong_min` < elongation < `elong_max`, `rout_min` cm < R_center < `rout_max` cm, `zout_min` cm < Z_center < `zout_max` cm)
        Current centroid (`rcurrt_min` cm < R_jcenter <  `rcurrt_max` cm, `zcurrt_min` cm < Z_jcenter < `zcurrt_max` cm) *should generally match `rout_*` and `zout_*`
        Kink safety factor (`qstar_min` < q* < `qstar_max`)
        Boundary Safety factor (`qout_min` < q_LCFS < `qout_max`)
        Plasma is within vessel (inboard gap > `gapin_min`, outboard gap > `gapout_min`, top gap > `gaptop_min`)
        Alternate calculations of beta_p+L_I/2 agree to within < `dbpli_diff`
        Alternate calculation of beta_p agree to within < `delbp_diff`

Use
---

Once the k-file inputs have been stored as described in `Requirements` there are only a
few steps required to launch the taskfarmer process:

  1. Run the task list setup script::

        setup_tasks.sh <dir>

  The arguement of this script is the full path to the parent directory where separate
  directories for each shot number are present.  If it executes successfully, you should see
  `tasks.txt` and `make_summary.sh` files created that have similar content to the 
  `example_tasks.txt` and `example_summary.sh` provided here.  If problems are encountered, 
  you should double check the argument passed to `setup_tasks.sh` and the directory structure
  where the k-files are located.  You can make or fix the `tasks.txt` and `make_summary.sh` 
  files by hand, but may run into errors later in the process by doing so.

  2. Make any other changes to the workflow scripts.  In general, no changes are required
       aside from changing the support files for a different device in line 2 of 
       `efit.input`.  

  3. Setup the batch scripts `farmer.sl` and `summary.sl`.  This should only require
       editing the allocation parameters at the top of the files (e.g. --queue, -N, and -t).

  4. Load the taskfarmer module (must happen before submitting any taskfarmer jobs)::

        module load taskfarmer

  5. Submit the job to the queue::

        sbatch farmer.sl

The terminal output from every process will be directed to the `farmer-{job_number}.out`
file.  If all processes complete within the requrested time, a file names
`done.tasks.txt.tfin` will be present in this directory.  If the timelimit was hit before
any could finish (or some hang), a `fastrecovery.tasks.txt.tfin` file will be here instead.
You can launch a new job that will automatically continue where the last one left off by
simply submitting `farmer.sl` to the queue again (make sure the taskfarmer module is still
loaded before submitting the new job).  The allocation request can also be changed before
the job is re-submitted (Note: it can take 10mins or more to restart the taskfarmer process
so there could be little to no progress observed in a queue as short as the debug). If you
want to re-run the taskfarmer process from the beginning, you can either run the
`setup_tasks.sh` script again or remove the following files before submitting a new job::

        tasks.txt.tfin
        done.tasks.txt.tfin
        fastrecovery.tasks.txt.tfin
        log.tasks.txt.tfin
        progress.tasks.txt.tfin

To queue/run multiple taskfarmer jobs at the same time (processing different datasets), you
should copy these files to a separate directory for each run.  This is the only way to avoid
conflicts.
