#!/usr/bin/env bash

# setup environment
export OMP_NUM_THREADS=1
export HDF5_USE_FILE_LOCKING=FALSE

# save location of taskfarmer files
iwd=$SLURM_SUBMIT_DIR

# move to directory where k-files are
cd $1/$2

# clean-up any unwanted, failed, or conflicting files
[ -f OMFITsave.txt ] && rm OMFITsave.txt 
[ -f efit.input ] && rm efit.input
[ -f temp.input ] && rm temp.input
files=`ls | grep g$2 | wc -l`
[ $files -gt 0 ] && rm g$2.*
files=`ls | grep a$2 | wc -l`
[ $files -gt 0 ] && rm a$2.*
files=`ls | grep m$2 | wc -l`
[ $files -gt 0 ] && rm m$2.*
[ -f errfil.out ] && rm errfil.out
[ -f fitout.dat ] && rm fitout.dat
[ -f esave.dat ] && rm esave.dat

# copy a clean input file here
cp $iwd/efit.input .

# collect number and names of k-files
steps=`ls k$2.* | wc -l`
files=`ls k$2.*`

# set-up the input file this run
sed 's/shot=/shot='$2'/g' efit.input > temp.input && mv temp.input efit.input
sed 's/steps=/steps='$steps'/g' efit.input > temp.input && mv temp.input efit.input
sed 's/inpfile=/inpfile=!next_file_goes_here/g' efit.input > temp.input && mv temp.input efit.input
for filename in $files; do
  sed 's/!next_file_goes_here/ "'$filename'",!next_file_goes_here/g' efit.input > temp.input && mv temp.input efit.input
done
sed 's/,!next_file_goes_here//g' efit.input > temp.input && mv temp.input efit.input

# execute EFIT
/global/common/software/efitai/cori/gnu_ser/efit/efit 257
mv *.h5 ..
